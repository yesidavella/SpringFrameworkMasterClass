package com.udemy.springcourse.maven;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.udemy.springcourse.maven.MavenApplicationTests.class)
public class MavenApplicationTests {

	@Test
	public void contextLoads() {

		System.out.println("Executing test from Yesid test folder");

	}

}
