package udemy.masterclass.spring.beans.componentScan;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE,
	proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ComponentConnection {

	public ComponentConnection() {
		System.out.println("Imprimiendo JDBCConnection");
	}

}
