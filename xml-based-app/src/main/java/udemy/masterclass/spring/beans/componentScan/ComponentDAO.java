package udemy.masterclass.spring.beans.componentScan;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ComponentDAO {

	@Autowired
	ComponentConnection jdbcConn;
	
	@PostConstruct
	public void postConstruct() {
		System.out.println("Call postConstruct() on ComponentDAO class");
	}
	
	public ComponentConnection getJdbcConn() {
		return jdbcConn;
	}

	public void setJdbcConn(ComponentConnection jdbcConn) {
		this.jdbcConn = jdbcConn;
	}
	
	@PreDestroy
	public void preDestroy() {
		System.out.println("Call preDestroy() on ComponentDAO class");
	}
}
