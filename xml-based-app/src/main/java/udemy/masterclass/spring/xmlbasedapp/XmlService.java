package udemy.masterclass.spring.xmlbasedapp;

import org.springframework.stereotype.Component;

@Component
public class XmlService {

	XmlDAO xmlDao;

//	@PostConstruct
//	public void postConstruct() {
//		System.out.println("Call postConstruct() on ComponentDAO class");
//	}
	
	public XmlDAO getXmlDao() {
		return xmlDao;
	}

	public void setXmlDao(XmlDAO xmlDao) {
		this.xmlDao = xmlDao;
	}
	
//	@PreDestroy
//	public void preDestroy() {
//		System.out.println("Call preDestroy() on ComponentDAO class");
//	}
}
