package udemy.masterclass.spring.xmlbasedapp;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XmlBasedAppRunner {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("appContext.xml");
		
		for(int i=0; i<appContext.getBeanDefinitionNames().length; i++) {
			System.out.println("Beans def:"+appContext.getBeanDefinitionNames()[i]);
		}
		
//		XmlService xmlService = appContext.getBean(XmlService.class);
		
//		System.out.println("xmlService:"+xmlService);
//		System.out.println("xmlDao:"+xmlService.getXmlDao());
		
	}
}
