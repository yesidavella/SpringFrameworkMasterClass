package yesid.service.demo;

import java.util.ArrayList;
import java.util.List;

public class Service {
	
	
	public List<String> getMsgFromService(String name){
		
		List<String> chain = new ArrayList<String>();
		chain.add("La primera parte ");
		chain.add(name);
		chain.add(" segunda parte");
		
		return chain;
	}

}
