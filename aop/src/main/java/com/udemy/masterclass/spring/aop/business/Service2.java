package com.udemy.masterclass.spring.aop.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udemy.masterclass.spring.aop.repository.Dao2;

@Service
public class Service2 {

	@Autowired
	public Dao2 dao2;
	
	public String getServiceDescription() {
		System.out.println("Llamando desde Service2 a Dao2");
		return dao2.getDaoName();
	}
}
