package com.udemy.masterclass.spring.aop.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udemy.masterclass.spring.aop.repository.Dao1;

@Service
public class Service1 {

	@Autowired
	public Dao1 dao1;
	
	public String getServiceDescription() {
		System.out.println("Llamando desde Service1 a Dao1");
		return dao1.getDaoName();
	}
}
