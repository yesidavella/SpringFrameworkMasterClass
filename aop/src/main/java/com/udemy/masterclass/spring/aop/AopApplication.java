package com.udemy.masterclass.spring.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.udemy.masterclass.spring.aop.business.Service1;
import com.udemy.masterclass.spring.aop.business.Service2;

@SpringBootApplication
public class AopApplication implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	public Service1 service1;
	
	@Autowired
	public Service2 service2;
	
	
	public static void main(String[] args) {
		SpringApplication.run(AopApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info(service1.getServiceDescription());
		logger.info(service2.getServiceDescription());
	}
}
