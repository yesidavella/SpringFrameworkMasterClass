package com.udemy.masterclass.spring.aop.repository;

import org.springframework.stereotype.Repository;

@Repository
public class Dao1 {

	public String getDaoName() {
		return "Mi nombre es Dao1";
	}
}
