package udemy.masterclass.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class NativeSpringAppWithJavaCdiApplication {

	public static void main(String[] args) {

		ApplicationContext appContext = new AnnotationConfigApplicationContext(NativeSpringAppWithJavaCdiApplication.class);
		//ApplicationContext appContext = SpringApplication.run(NativeSpringAppWithJavaCdiApplication.class);

		CDIService cdiService = appContext.getBean(CDIService.class);
		CDIDAO cdiDao = appContext.getBean(CDIDAO.class);
		
		System.out.println("cdiService:"+cdiService);
		System.out.println("cdiDao:"+cdiDao);
	}
}