package udemy.masterclass.spring;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@Named
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CDIService {

	@Inject
	CDIDAO cdiDao;

//	@PostConstruct
//	public void postConstruct() {
//		System.out.println("Call postConstruct() on ComponentDAO class");
//	}
	
	public CDIDAO getCdiDao() {
		return cdiDao;
	}

	public void setCdiDao(CDIDAO cdiDao) {
		this.cdiDao = cdiDao;
	}
	
//	@PreDestroy
//	public void preDestroy() {
//		System.out.println("Call preDestroy() on ComponentDAO class");
//	}
}
