package udemy.masterclass.spring.nativeSpringAppWithJavaCDI;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=NativeSpringAppWithJavaCdiApplicationTests.class)
public class NativeSpringAppWithJavaCdiApplicationTests {

	@Test
	public void contextLoads() {
		System.out.println("Ejecutando pruebas Junit");
		assertTrue(true);
	}
}