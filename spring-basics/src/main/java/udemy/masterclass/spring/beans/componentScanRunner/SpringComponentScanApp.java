package udemy.masterclass.spring.beans.componentScanRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import udemy.masterclass.spring.beans.componentScan.ComponentDAO;
import udemy.masterclass.spring.beans.scope.PersonDAO;

@SpringBootApplication
@ComponentScan({"udemy.masterclass.spring.beans.componentScan","udemy.masterclass.spring.beans.scope"})
@PropertySource("classpath:application.properties")
public class SpringComponentScanApp {

	public static Logger LOGGER = LoggerFactory.getLogger(SpringComponentScanApp.class);
	
	public static void main(String[] args) {

		ApplicationContext appContext = SpringApplication.run(SpringComponentScanApp.class);

		ComponentDAO componentDAO = appContext.getBean(ComponentDAO.class);
		
		PersonDAO personDAO = appContext.getBean(PersonDAO.class);

		
//		No se que rayos hace este log bien
		//LOGGER.info("{}",componentDAO);
	}
}