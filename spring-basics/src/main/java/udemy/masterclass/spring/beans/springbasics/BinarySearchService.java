package udemy.masterclass.spring.beans.springbasics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import udemy.masterclass.spring.beans.springbasics.algorithms.SortAlgorithm;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BinarySearchService {

	@Autowired
	@Qualifier("quickAlg")
	SortAlgorithm sortAlg;

//	public BinarySearchService(SortAlgorithm bubbleSortAlgorithm) {
//		super();
//		this.bubbleSortAlgorithm = bubbleSortAlgorithm;
//	}

	public int binarySearch(int[] numbers, int numberToSearchFor) {

		int foundNumber = sortAlg.sort(numbers, numberToSearchFor);

		System.out.println("The selected sort algorithm is:" + sortAlg);

		return foundNumber;
	}

}
