package udemy.masterclass.spring.beans.scope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class PersonDAO {

	@Autowired
	JDBCConnection jdbcConn;

	public JDBCConnection getJdbcConn() {
		return jdbcConn;
	}

	public void setJdbcConn(JDBCConnection jdbcConn) {
		this.jdbcConn = jdbcConn;
	}
	
}
