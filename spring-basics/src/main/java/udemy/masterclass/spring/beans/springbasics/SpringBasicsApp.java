package udemy.masterclass.spring.beans.springbasics;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBasicsApp {

	public static void main(String[] args) {

		ApplicationContext appContext = SpringApplication.run(SpringBasicsApp.class);

		BinarySearchService searchService1 = appContext.getBean(BinarySearchService.class);
		BinarySearchService searchService2 = appContext.getBean(BinarySearchService.class);
		
		System.out.println("searchService1:"+searchService1);
		System.out.println("searchService2:"+searchService2);
		
//		int selectedNumber = searchService1.binarySearch(new int[] { 1, 2, 3, 4 }, 3);
//		
//		System.out.println("The selected number is:"+selectedNumber);
	}
}