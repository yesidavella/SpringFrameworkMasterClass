package udemy.masterclass.spring.beans.scope;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import udemy.masterclass.spring.beans.componentScan.ComponentDAO;

@SpringBootApplication
@ComponentScan("udemy.masterclass.spring.beans.componentScan")

public class SpringScopeApp {

	public static Logger LOGGER = LoggerFactory.getLogger(SpringScopeApp.class);
	
	public static void main(String[] args) {

		ApplicationContext appContext = SpringApplication.run(SpringScopeApp.class);

		PersonDAO personDAO = appContext.getBean(PersonDAO.class);
		PersonDAO personDAO2 = appContext.getBean(PersonDAO.class);

		//ComponentDAO componentDAO = appContext.getBean(ComponentDAO.class);

		
		System.out.println("personDAO:" + personDAO);
		System.out.println("JdbcConn:" + personDAO.getJdbcConn());

		
		System.out.println("personDAO2:" + personDAO2);		
		System.out.println("JdbcConn de PersonDAO2:" + personDAO2.getJdbcConn());
		
//		No se que rayos hace este log bien
		LOGGER.info("{}",personDAO2);
	}
}