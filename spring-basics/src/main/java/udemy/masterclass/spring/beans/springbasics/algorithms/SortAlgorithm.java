package udemy.masterclass.spring.beans.springbasics.algorithms;

public interface SortAlgorithm {

	public int sort(int[] numbers, int numberToFind);
	
}
